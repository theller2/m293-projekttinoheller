//Für Warenkorb:
const cartContainer = document.createElement("div");
  cartContainer.id = "cart-container";
  cartContainer.innerHTML = `
    <p class="carticon" id="cart-icon" alt="Cart"><img src ="Bilder/shoppingcart.png" class="cart"></p><span class="carticon headingwrapper" id="cart-count">0</span>`;
  
  const titleContainer = document.querySelector(".saladtitle");
  titleContainer.appendChild(cartContainer);

  var cartCount = 0;

  function updateCartCount() {
    const cartCountElement = document.getElementById("cart-count");
    cartCountElement.textContent = cartCount.toString();
  }

  function resetCart() {    
    alert("Besten Dank für ihre Bestellung!");
    cartCount = 0;
    updateCartCount();
}

  function warenkorb() {
    cartCount++;
    updateCartCount();
  }

//Event-Listener für Container:
document.addEventListener("DOMContentLoaded", function () {
    const pizzaContainer = document.querySelector(".pizzacontainer");

    //Warenkorb:
    pizzaContainer.addEventListener("click", function (event) {
      if (event.target.classList.contains("cart")) {
          warenkorb();
      }
  });

  //Reset-Cart
  const cartIcon = document.querySelector("#cart-icon img");
    cartIcon.addEventListener("click", function () {
        resetCart();
    })
    
    //Einfügen der Elemente:
    function addPizzaToContainer(pizza) {
      const content = document.createElement("div");
      content.classList.add("content");
  
      const pizzaImage = document.createElement("div");
      pizzaImage.classList.add("row", "pizza_img");
      pizzaImage.innerHTML = `<img src="${pizza.imageUrl}" class="PizzaImage">`;
      content.appendChild(pizzaImage);
  
      const pizzaInfo = document.createElement("div");
      pizzaInfo.classList.add("row");
      pizzaInfo.innerHTML = `
        <div class="item1">${pizza.name}</div>
        <div class="item2">${pizza.prize}</div>
        <div class="item3">
          <button class="cart">
          <img src="Bilder/shoppingcart.png" class="cart" alt="Add to Cart">
          </button>
       </div>
      `;
      content.appendChild(pizzaInfo);
  
      const pizzaIngredients = document.createElement("div");
      pizzaIngredients.classList.add("row", "item3");
      pizzaIngredients.textContent = pizza.ingredients.join(", ");
      content.appendChild(pizzaIngredients);
  
      pizzaContainer.appendChild(content);
    }
  
    fetch("/Data/pizzas.json")
      .then((response) => response.json())
      .then((pizzaData) => {
        pizzaData.forEach(addPizzaToContainer);
      })  
  });