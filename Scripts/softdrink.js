//Warenkorb:
const cartContainer = document.createElement("div");
  cartContainer.id = "cart-container";
  cartContainer.innerHTML = `
  <p class="carticon" id="cart-icon" alt="Cart"><img src ="Bilder/shoppingcart.png" class="cart"></p><span class="carticon headingwrapper" id="cart-count">0</span>`;
  
  const titleContainer = document.querySelector(".saladtitle");
  titleContainer.appendChild(cartContainer);

  let cartCount = 0;

  function updateCartCount() 
  {
    const cartCountElement = document.getElementById("cart-count");
    cartCountElement.textContent = cartCount.toString();
  }

  function resetCart() 
  {    
    alert("Besten Dank für ihre Bestellung!");
    cartCount = 0;
    updateCartCount();
}

  function warenkorb() 
  {
    cartCount++;
    updateCartCount();
  }

//Event-Listener für Container:
document.addEventListener("DOMContentLoaded", function () {
    const softdrinksContainer = document.querySelector(".pizzacontainer");

    softdrinksContainer.addEventListener("click", function (event) {
      if (event.target.classList.contains("cart")) {
          warenkorb();
      }
  }); 

  const cartIcon = document.querySelector("#cart-icon img");
    cartIcon.addEventListener("click", function () {
        resetCart();
    })
    
    //Einfügen der Elemente:
    function addSoftdrinksToContainer(softdrink) {
      const content = document.createElement("div");
      content.classList.add("content");
  
      const softdrinksImage = document.createElement("div");
      softdrinksImage.classList.add("row", "pizza_img");
     softdrinksImage.innerHTML = `<img src="${softdrink.imageUrl}" class="PizzaImage">`;
      content.appendChild(softdrinksImage);
  
      const softdrinkInfo = document.createElement("div");
      softdrinkInfo.classList.add("row");
      softdrinkInfo.innerHTML = `
        <div class="item1">${softdrink.name}</div>
      `;
      content.appendChild(softdrinkInfo);
  
      const softdrinkDetails = document.createElement("div");
      softdrinkDetails.classList.add("row");
      softdrinkDetails.innerHTML = `
        <div class="dressing">
          <select>
            <option value="french_dressing">${softdrink.volume}</option>           
          </select>
        </div>
        <div class="item2">${softdrink.prize}</div>
        <div class="item3">
          <button onclick="warenkorb()" class="cart">
          <img src="Bilder/shoppingcart.png" class="cart" alt="Add to Cart">
          </button>
       </div>
      `;
      content.appendChild(softdrinkDetails);
  
      softdrinksContainer.appendChild(content);
    }
  
    fetch("/Data/softdrinks.json")
      .then((response) => response.json())
      .then((softdrinkData) => {
        softdrinkData.forEach(addSoftdrinksToContainer);
      })
      .catch((error) => console.error("Fehler beim Laden der Daten: ", error));
  });
