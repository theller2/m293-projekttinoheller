//Für Warenkorb:
const cartContainer = document.createElement("div");
  cartContainer.id = "cart-container";
  cartContainer.innerHTML = `
  <p class="carticon" id="cart-icon" alt="Cart"><img src ="Bilder/shoppingcart.png" class="cart"></p><span class="carticon headingwrapper" id="cart-count">0</span>`;
  
  const titleContainer = document.querySelector(".saladtitle");
  titleContainer.appendChild(cartContainer);

  let cartCount = 0;

  function updateCartCount() 
  {
    const cartCountElement = document.getElementById("cart-count");
    cartCountElement.textContent = cartCount.toString();
  }

  function resetCart() 
  {    
    alert("Besten Dank für ihre Bestellung!");
    cartCount = 0;
    updateCartCount();
  } 

  function warenkorb() 
  {
    cartCount++;
    updateCartCount();
  }

  //Event-Listener für Container:
document.addEventListener("DOMContentLoaded", function () {
    const saladContainer = document.querySelector(".pizzacontainer");

    saladContainer.addEventListener("click", function (event) {
      if (event.target.classList.contains("cart")) {
          warenkorb();
      }
  }); 

  const cartIcon = document.querySelector("#cart-icon img");
    cartIcon.addEventListener("click", function () {
        resetCart();
    })
  
    //Einfügen der Elemente:
    function addSaladToContainer(salad) {
      const content = document.createElement("div");
      content.classList.add("content");
  
      const saladImage = document.createElement("div");
      saladImage.classList.add("row", "pizza_img");
      saladImage.innerHTML = `<img src="${salad.imageUrl}" class="PizzaImage">`;
      content.appendChild(saladImage);
  
      const saladInfo = document.createElement("div");
      saladInfo.classList.add("row");
      saladInfo.innerHTML = `
        <div class="item1">${salad.name}</div>
      `;
      content.appendChild(saladInfo);
  
      const saladIngredients = document.createElement("div");
      saladIngredients.classList.add("row");
      saladIngredients.innerHTML = `
        <div class="item3">${salad.ingredients.join(", ")}</div>
      `;
      content.appendChild(saladIngredients);
  
      const saladDetails = document.createElement("div");
      saladDetails.classList.add("row");
      saladDetails.innerHTML = `
        <div class="dressing">
          <select>
            <option value="italian_dressing">${salad.dressing}</option>
          </select>
        </div>
        <div class="item2">${salad.prize}</div>
        <div class="item3">
          <button onclick="warenkorb()" class="cart">
          <img src="Bilder/shoppingcart.png" class="cart" alt="Add to Cart">
          </button>
       </div>
      `;
    
  
    
      content.appendChild(saladDetails);
  
      saladContainer.appendChild(content);
    }
  
    fetch("/Data/salads.json")
      .then((response) => response.json())
      .then((saladData) => {
        saladData.forEach(addSaladToContainer);
      })
      .catch((error) => console.error("Fehler beim Laden der Daten: ", error));
  });